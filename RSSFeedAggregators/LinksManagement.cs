﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSSFeedAggregators
{
  public class LinksManagement
  {
    private Dictionary<string, Model.Feed> feedSaved;

    public LinksManagement()
    {
      this.feedSaved = new Dictionary<string, Model.Feed>();
    }

    public async Task AddNewLink(string[] input)
    {
      if (input.Length < 2)
        throw new Exception("Add command should have a link; 'add link'");
      
      if (this.feedSaved.ContainsKey(input[1]))
        throw new Exception("RSS link already added");

      try
      {
        var xml = await RSSLinkFetcher.FetchContent(input[1]);

        if (!String.IsNullOrEmpty(xml))
        {
          var parser = new Rss.RssParser(xml);
          var feed = parser.GetHeader();

          this.feedSaved.Add(input[1], feed);
        }
      }
      catch (Exception)
      {
        throw new Exception("RSS link invalid");
      }
    }    
  }
}
