﻿using System;

namespace RSSFeedAggregators
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Welcome to RSSFeedAggregator!");

      var consoleAdmin = new ConsoleAdmin();
      consoleAdmin.Run();
    }
  }
}
