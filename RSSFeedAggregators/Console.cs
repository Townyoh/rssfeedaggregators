﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSSFeedAggregators
{
  public class ConsoleAdmin
  {
    Core applicationCore;

    public ConsoleAdmin()
    {
      this.applicationCore = new Core();
    }

    public void Run()
    {
      bool shouldRun = true;
      var input = new String("");
      do
      {
        input = Console.ReadLine();

        var task = applicationCore.CommandDispatcher(input);
        task.Wait();

        shouldRun = task.Result;
      } while (shouldRun);
    }
  }
}
