﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

namespace RSSFeedAggregators
{
  public static class RSSLinkFetcher
  {
    public static async Task<string> FetchContent(string linkToFetch)
    {
      var content = new String("");

      using (HttpClient client = new HttpClient())
      {
        using (HttpResponseMessage response = await client.GetAsync(linkToFetch).ConfigureAwait(false))
        {
          using (HttpContent httpContent = response.Content)
          {
            content = await httpContent.ReadAsStringAsync();
          }
        }
      }

      return content;
    }
  }
}
