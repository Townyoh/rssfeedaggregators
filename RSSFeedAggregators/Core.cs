﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSSFeedAggregators
{
  public class Core
  {
    readonly char[] delimiter = { ' ', '\t' };
    readonly string quitCommand = "!quit";
    readonly string addCommand = "add";

    LinksManagement linksManagement;

    public Core()
    {
      linksManagement = new LinksManagement();
    }

    //public bool CommandDispatcher(string input)
    public async Task<bool> CommandDispatcher(string input)
    {
      bool shouldRun = true;

      try
      {
        var inputList = input.Split(this.delimiter, StringSplitOptions.RemoveEmptyEntries);

        if (inputList == null || inputList.Length == 0)
          shouldRun = true;
        else if (inputList.Length > 2)
          Console.WriteLine("Command not found");
        else if (string.Equals(inputList[0], this.quitCommand))
          shouldRun = false;
        else if (string.Equals(inputList[0], this.addCommand))
          await this.linksManagement.AddNewLink(inputList);
        else
          Console.WriteLine("Command not found");
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }

      return shouldRun;
    }

  }
}
