﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace RSSFeedAggregators.Rss
{
  public class RssParser
  {
    readonly private RssReader reader;

    private readonly string title = "title";
    private readonly string link = "link";
    private readonly string description = "description";
    private readonly string language = "language";
    private readonly string category = "category";

    public RssParser(string xml)
    {
      this.reader = new RssReader(xml);
    }

    public Model.Feed GetHeader()
    {
      var title = this.Sanitize(reader.GetNodeValue(this.title));
      var link = this.Sanitize(reader.GetNodeValue(this.link));
      var description = this.Sanitize(reader.GetNodeValue(this.description));
      var language = this.Sanitize(reader.GetNodeValue(this.language));
      var category = this.Sanitize(reader.GetNodeValue(this.category));

      var feed = new Model.Feed(title, link, description, language, category);

      return feed;
    }

    private string Sanitize(XmlNode nodeValue)
    {
      if (nodeValue == null)
        return new string("");

      return nodeValue.InnerText.Trim();
    }
  }
}
