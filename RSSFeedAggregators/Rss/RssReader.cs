﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace RSSFeedAggregators.Rss
{
  public class RssReader
  {
    XmlDocument document;
    Dictionary<string, XmlNode> header;
    List<XmlNode> itemList;
    readonly HashSet<string> nodeToCatch;

    public RssReader(string xml)
    {
      string[] catchElem = { "item", "title", "link", "description", "language", "category" };
      this.nodeToCatch = new HashSet<string>(catchElem);
      this.header = new Dictionary<string, XmlNode>();
      this.itemList = new List<XmlNode>();
      this.document = new XmlDocument();
      this.document.LoadXml(xml);

      this.BrowseRssFile();
    }

    public XmlNode GetNodeValue(string nodeName)
    {
      XmlNode value = null;

      this.header.TryGetValue(nodeName, out value);

      return value;
    }

    public IEnumerable<XmlNode> GetItem()
    {
      foreach (var node in this.itemList)
      {
        yield return node;
      }
    }

    private void BrowseRssFile()
    {
      Queue<XmlNode> fifo = new Queue<XmlNode>();
      this.AddAllChilds(fifo, this.document);
      
      while (fifo.Count > 0)
      {
        XmlNode node = fifo.Dequeue();

        if (this.nodeToCatch.Contains(node.Name))
          this.AddContentInformation(node);
        else
        {
          this.AddAllChilds(fifo, node);
        }
      }
    }

    private void AddAllChilds(Queue<XmlNode> fifo, XmlNode node)
    {
      foreach (XmlNode child in node.ChildNodes)
      {
        fifo.Enqueue(child);
      }
    }

    private void AddContentInformation(XmlNode node)
    {
      if (node.Name == "item")
        this.itemList.Add(node);
      else
        this.header.Add(node.Name, node);
    }
  }
}
