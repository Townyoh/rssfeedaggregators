﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSSFeedAggregators.Model
{
  public struct Feed
  {
    public readonly string title;
    public readonly string link;
    public readonly string description;
    public readonly string language;
    public readonly string category;
    //public DateTime lastBuild;

    public Feed(string title, string link, string description, string language, string category)
    {
      this.title = title;
      this.link = link;
      this.description = description;
      this.language = language;
      this.category = category;
    }
  }
}
