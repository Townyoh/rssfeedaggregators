# RSSFeedAggregators

**Functionnality**

For now, only command line.
Functionnality:  
*  help => display all command
*  list => display current directory.
    * display all feed
    * display all news
*  select "feed" => select a feed
*  read "news" => open browser with current news
*  hide "news" => mark as read a news
*  back => go back to feed selection
*  add "link" => add a new feed by rss link
*  delete "feed" => delete an already added feed



