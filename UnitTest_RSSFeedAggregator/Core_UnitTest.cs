using NUnit.Framework;
using System;
using System.Threading.Tasks;

using RSSFeedAggregators;
using System.IO;

namespace Core_UnitTest
{
  [TestFixture]
  public class CommandDispatcher_UnitTest
  {
    [Test]
    public void QuitCommandAsync()
    {
      var input = new String("!quit");
      Core core = new RSSFeedAggregators.Core();

      async Task<bool> lambda() => await core.CommandDispatcher(input);

      var task = lambda();
      Assert.False(task.Result);
    }

    [Test]
    public void EmptyInput()
    {
      var input = new String("");
      Core core = new RSSFeedAggregators.Core();

      async Task<bool> lambda() => await core.CommandDispatcher(input);

      var task = lambda();
      Assert.True(task.Result);
    }

    [Test]
    public void CommandNotFound()
    {
      var input = new String("aze  azeaz");
      Core core = new RSSFeedAggregators.Core();

      StringWriter writer = new StringWriter();
      Console.SetOut(writer);

      async Task<bool> lambda() => await core.CommandDispatcher(input);
      var task = lambda();

      string expected = string.Format("Command not found{0}", Environment.NewLine);
      Assert.AreEqual(writer.ToString(), expected);
    }

    [Test]
    public void AddNewFeed()
    {
      var input = new String("add http://www.jeuxvideo.com/rss/rss.xml");
      Core core = new RSSFeedAggregators.Core();

      async Task<bool> lambda() => await core.CommandDispatcher(input);
      var task = lambda();

      Assert.True(task.Result);
    }

    [Test]
    public void AddCommandMissingLink()
    {
      var input = new String("add");
      Core core = new RSSFeedAggregators.Core();

      StringWriter writer = new StringWriter();
      Console.SetOut(writer);

      async Task<bool> lambda() => await core.CommandDispatcher(input);
      var task = lambda();

      string expected = string.Format("Add command should have a link; 'add link'{0}", Environment.NewLine);
      Assert.AreEqual(writer.ToString(), expected);
    }
  }
}