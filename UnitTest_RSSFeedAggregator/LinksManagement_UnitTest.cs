﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using RSSFeedAggregators;

namespace LinksManagement_UnitTest
{
  [TestFixture]
  public class AddNewLink_UnitTest
  {
    [Test]
    public void WrongRssLink()
    {
      var input = new string[] { "add", "link" };
      var manager = new LinksManagement();

      var ex = Assert.ThrowsAsync<Exception>(async () => await manager.AddNewLink(input));
      Assert.AreEqual(ex.Message, "RSS link invalid");
    }

    [Test]
    public void NotEnoughtArgument()
    {
      var input = new string[] { "add" };
      var manager = new LinksManagement();

      var ex = Assert.ThrowsAsync<Exception>(async () => await manager.AddNewLink(input));
      Assert.AreEqual(ex.Message, "Add command should have a link; 'add link'");
    }

    [Test]
    public void RssLinkAlreadyAdded()
    {
      var input = new string[] { "add", "http://www.jeuxvideo.com/rss/rss.xml" };
      var manager = new LinksManagement();

      Assert.DoesNotThrowAsync(async () => await manager.AddNewLink(input));

      var ex = Assert.ThrowsAsync<Exception>(async () => await manager.AddNewLink(input));
      Assert.AreEqual(ex.Message, "RSS link already added");
    }
  }
}
