﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using RSSFeedAggregators.Rss;
using RSSFeedAggregators.Model;

namespace XmlParser_UnitTest
{
  [TestFixture]
  public class GetHeader_UnitTest
  {
    [Test]
    public void GetHeaderFromXmlFile()
    {
      var file = new string("..\\..\\..\\XmlFiles\\JeuxVideo.com_2019-07-13.xml");

      if (!File.Exists(file))
      {
        Assert.Warn("File {0} do not exist", file);
        return;
      }

      var xml = File.ReadAllText(file);
      var parser = new RssParser(xml);
      var feed = parser.GetHeader();

      Assert.AreEqual(feed.title, "jeuxvideo.com");
      Assert.AreEqual(feed.link, "http://www.jeuxvideo.com/");
      Assert.AreEqual(feed.description, "La Référence de Jeux Vidéo PC et Consoles");
      Assert.AreEqual(feed.language, "fr-fr");
      Assert.AreEqual(feed.category, "Jeux Video");
    }

    [Test]
    public void GetHeaderFromXmlWithEmptyInfo()
    {
      var file = new string("..\\..\\..\\XmlFiles\\MangaNews.com_2019-07-13.xml");

      if (!File.Exists(file))
      {
        Assert.Warn("File {0} do not exist", file);
        return;
      }

      var xml = File.ReadAllText(file);
      var parser = new RssParser(xml);
      var feed = parser.GetHeader();

      Assert.AreEqual(feed.title, "Manganews - Actus");
      Assert.AreEqual(feed.link, "https://www.manga-news.com/");
      Assert.AreEqual(feed.description, "Toute l'actualité du manga : présentation de toutes les séries sorties en France, le planning, les résumés, les auteurs, les éditeurs, manga en ligne, dossiers...");
      Assert.AreEqual(feed.language, "Fr");
      Assert.IsEmpty(feed.category);
    }
  }
}
