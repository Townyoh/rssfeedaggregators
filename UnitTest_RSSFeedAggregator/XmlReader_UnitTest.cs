﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using RSSFeedAggregators.Rss;

namespace XmlReader_UnitTest
{
  [TestFixture]
  class GetNodeValue_UnitTest
  {
    [Test]
    public void GetRealNode()
    {
      var file = new string("..\\..\\..\\XmlFiles\\JeuxVideo.com_2019-07-13.xml");

      if (!File.Exists(file))
      {
        Assert.Warn("File {0} do not exist", file);
        return;
      }

      var xml = File.ReadAllText(file);
      RssReader reader = new RssReader(xml);

      Assert.AreEqual(reader.GetNodeValue("title").InnerText.Trim(), "jeuxvideo.com");
      Assert.AreEqual(reader.GetNodeValue("link").InnerText.Trim(), "http://www.jeuxvideo.com/");
      Assert.AreEqual(reader.GetNodeValue("description").InnerText.Trim(), "La Référence de Jeux Vidéo PC et Consoles");
      Assert.AreEqual(reader.GetNodeValue("language").InnerText.Trim(), "fr-fr");
      Assert.AreEqual(reader.GetNodeValue("category").InnerText.Trim(), "Jeux Video");
    }

    [Test]
    public void GetRealNodeOtherFile()
    {
      var file = new string("..\\..\\..\\XmlFiles\\MangaNews.com_2019-07-13.xml");

      if (!File.Exists(file))
      {
        Assert.Warn("File {0} do not exist", file);
        return;
      }

      var xml = File.ReadAllText(file);
      RssReader reader = new RssReader(xml);

      Assert.AreEqual(reader.GetNodeValue("title").InnerText.Trim(), "Manganews - Actus");
      Assert.AreEqual(reader.GetNodeValue("link").InnerText.Trim(), "https://www.manga-news.com/");
      Assert.AreEqual(reader.GetNodeValue("description").InnerText.Trim(), "Toute l'actualité du manga : présentation de toutes les séries sorties en France, le planning, les résumés, les auteurs, les éditeurs, manga en ligne, dossiers...");
      Assert.AreEqual(reader.GetNodeValue("language").InnerText.Trim(), "Fr");
      Assert.IsNull(reader.GetNodeValue("category"));
    }
  }
}
